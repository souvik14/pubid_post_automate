package com.morrisons.jda3b.automataion.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.morrisons.jda3b.automataion.config.ApplicationConfig;
import com.morrisons.jda3b.automataion.dto.Payload;
import com.morrisons.jda3b.automataion.util.RDSUtil;

public class PayloadProcessorDao {
	
	
	
	
	public void updatePayloadStatus(ApplicationConfig config, RDSUtil util, String pubId) {
		Connection conn = null;
		ResultSet rsSet = null;
		String payload = null;
		Payload readValue= null;
		
		PreparedStatement preStatement = null;
		try{
			conn = util.getRDSConnection(config);
			System.out.println("FetchQuery ::" + config.getUpdateQuery()+pubId);
			preStatement = conn.prepareStatement(config.getUpdateQuery()+pubId);	
			preStatement.setString(1, "C");
			int executeUpdate = preStatement.executeUpdate();
			System.out.println("update status..."+ executeUpdate);
		
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			util.closeConnection(conn, preStatement, rsSet);
		}
		
	}

	public Payload fetchPayload(ApplicationConfig config, RDSUtil util, String pubId) {
		Connection conn = null;
		ResultSet rsSet = null;
		String payload = null;
		Payload readValue= null;
		
		PreparedStatement preStatement = null;
		try{
			conn = util.getRDSConnection(config);
			System.out.println("FetchQuery ::" + config.getFetchPayloadQuery()+pubId);
			preStatement = conn.prepareStatement(config.getFetchPayloadQuery()+pubId);	
			rsSet = preStatement.executeQuery();
			while(rsSet.next()){
				payload = rsSet.getString("payload");
				System.out.println("payload is " + payload);
			}
			ObjectMapper mapper = new ObjectMapper();
			readValue = mapper.readValue(payload, Payload.class);
			System.out.println(readValue);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			util.closeConnection(conn, preStatement, rsSet);
		}
		return readValue;
	}
}
