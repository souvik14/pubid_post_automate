package com.morrisons.jda3b.automataion.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Items {

	private Double qty;
	private String item;
	private String reason;
	private String internal;
	private String adjustedAt;
	
	
}
