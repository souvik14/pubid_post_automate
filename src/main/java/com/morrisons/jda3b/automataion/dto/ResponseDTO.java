package com.morrisons.jda3b.automataion.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDTO {
	
	private int responseCode;
	private String responseBody;

}
