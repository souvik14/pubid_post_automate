package com.morrisons.jda3b.automataion.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Payload {
	
	private List<Items> items;
	private String messageId;
	
	
	public void addAllPayload(List<Items> itemsList){
		if(items ==null){
			items = new ArrayList<Items>();
		}if(itemsList!=null){
			items.addAll(itemsList);
		}
	}

}
