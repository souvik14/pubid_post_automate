package com.morrisons.jda3b.automataion.processor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.apache.http.HttpStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.morrisons.jda3b.automataion.config.ApplicationConfig;
import com.morrisons.jda3b.automataion.dao.PayloadProcessorDao;
import com.morrisons.jda3b.automataion.dto.Items;
import com.morrisons.jda3b.automataion.dto.Payload;
import com.morrisons.jda3b.automataion.dto.ResponseDTO;
import com.morrisons.jda3b.automataion.util.HttpClientUtil;
import com.morrisons.jda3b.automataion.util.RDSUtil;

public class ApplicationProcessor {

	
	
	ApplicationConfig config;
	RDSUtil util;
	HttpClientUtil clientUtil;
	String pubId;
	String storeNo;
	
	public ApplicationProcessor(ApplicationConfig config, RDSUtil util,HttpClientUtil clientUtil,String pubId,String storeNo) {
		this.config = config;
		this.util = util;
		this.clientUtil = clientUtil;
		this.pubId = pubId;
		this.storeNo = storeNo;
	}


	public void processData() {
	
		PayloadProcessorDao processorDao = new PayloadProcessorDao();
		processorDao.updatePayloadStatus(config, util,pubId);
		Payload fetchPayload = processorDao.fetchPayload(config, util,pubId);
		System.out.println("payload length:::" + fetchPayload.getItems().size());
		int presetSize = fetchPayload.getItems()!=null ? fetchPayload.getItems().size() :0;
		int initialSize = 0;
		int finalSize = Integer.valueOf(config.getFilterSize());
		List<Items> tobePosted = null;
		Payload payLoadDTO = null;
		int counter = 1;
		while (presetSize > 0) {
			try {
				if (presetSize < finalSize) {
					tobePosted = fetchPayload.getItems().subList(initialSize, fetchPayload.getItems().size());
				} else {
					tobePosted = fetchPayload.getItems().subList(initialSize, finalSize);
				}
				payLoadDTO = new Payload();
				payLoadDTO.addAllPayload(tobePosted);
				payLoadDTO.setMessageId(fetchPayload.getMessageId());
				if (payLoadDTO.getItems() != null) {
					// post call to stock v2
					ObjectMapper mapper = new ObjectMapper();
					String jsonString;
					jsonString = mapper.writeValueAsString(payLoadDTO);
					System.out.println( counter +" JsonString is :::" + jsonString);
					/*ResponseDTO postResponse = clientUtil.composePOSTRequest(config.getApiUrl(), jsonString, config.getAuthorization());
					if(postResponse!=null && postResponse.getResponseCode() == HttpStatus.SC_CREATED){
						
						System.out.println("response successfully posted");
					}*/
				}
				initialSize = finalSize;
				finalSize = finalSize + Integer.valueOf(config.getFilterSize());
				presetSize = presetSize - Integer.valueOf(config.getFilterSize());
				counter++;
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
		
	}

}
