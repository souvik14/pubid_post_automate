package com.morrisons.jda3b.automataion.config;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;


public class ConfigLoader {
	

	private static final String ENV_KEY = "xxwmm_env";
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(ConfigLoader.class);
	private ApplicationConfig appConfiguration = null;
	private static final Map<String, String> envSpecificConfig = new HashMap<String, String>();
		
	public ConfigLoader() {
		envSpecificConfig.put("dev", "application_DEV.yml");
		envSpecificConfig.put("sit", "application_SIT.yml");
		try {
			String env = System.getenv(ENV_KEY);
			String tempFileName = envSpecificConfig.get(env != null ? env.toLowerCase() : "dev");
			logger.info("temp file name :: {}" , tempFileName);
			appConfiguration = new Yaml().loadAs(
					ApplicationConfig.class.getClassLoader().getResourceAsStream(tempFileName),
					ApplicationConfig.class);
			logger.info("Configuration loading success");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured while configuring: ", e);
			System.exit(-1);
		}
	}

	public ApplicationConfig getConfig() {
		return appConfiguration;
	}

}
