package com.morrisons.jda3b.automataion.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationConfig {
	private String hostName;
	private String rdsUserName;
	private String rdsPassword;
	private String rdsPort;
	private String rdsDbName;
	private String updateQuery;
	private String fetchPayloadQuery;
	private String filterSize;
	private String apiUrl;
	private String authorization;
	

}
