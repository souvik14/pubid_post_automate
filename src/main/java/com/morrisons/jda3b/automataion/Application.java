package com.morrisons.jda3b.automataion;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.morrisons.jda3b.automataion.config.ApplicationConfig;
import com.morrisons.jda3b.automataion.processor.ApplicationProcessor;
import com.morrisons.jda3b.automataion.util.HttpClientUtil;
import com.morrisons.jda3b.automataion.util.RDSUtil;


public class Application 
{
	
	 private static final Logger logger = LogManager.getLogger(Application.class);

	public static ApplicationConfig loadPropertiesFile(String configPath) throws IOException {
		
		ApplicationConfig config = new ApplicationConfig();
		Properties prop  = new Properties();
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(configPath);
		prop.load(input);
		config.setHostName(prop.getProperty("RDS_HOSTNAME"));
		config.setRdsPassword(prop.getProperty("RDS_USERNAME"));
		config.setRdsUserName(prop.getProperty("RDS_PASSWORD"));
		config.setRdsPort(prop.getProperty("RDS_PORT"));
		config.setRdsDbName(prop.getProperty("RDS_DBNAME"));
		config.setFetchPayloadQuery(prop.getProperty("FETCH_QUERY"));
		config.setFilterSize(prop.getProperty("FILTER_SIZE"));
		config.setApiUrl(prop.getProperty("STOCKV2_URL"));
		config.setAuthorization(prop.getProperty("AUTHORIZATION"));
		return config;
	}
	    
	
    public static void main( String[] args )
    {
    
    	ApplicationConfig config = null;
		try {
			config = loadPropertiesFile("application_local.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}
		String pubId = args[0];
		String StoreNo = args[1];
		RDSUtil dbUtil = new RDSUtil();
		HttpClientUtil clientUtil = new HttpClientUtil();
		ApplicationProcessor procesor = new ApplicationProcessor(config,dbUtil,clientUtil,pubId,StoreNo);
        procesor.processData();
	
    }

}