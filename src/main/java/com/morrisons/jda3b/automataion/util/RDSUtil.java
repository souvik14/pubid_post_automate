package com.morrisons.jda3b.automataion.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.morrisons.jda3b.automataion.config.ApplicationConfig;


public class RDSUtil {
	
	
	
	public Connection getRDSConnection(ApplicationConfig appConfig) {
		
		  Connection con = null;
	   
	      try {
	      String dbName = appConfig.getRdsDbName();
	      String hostname = appConfig.getHostName();
	      String userName =  appConfig.getRdsUserName();
	      String password = appConfig.getRdsPassword();
	      String port = appConfig.getRdsPort();
	      
	      
	      String jdbcUrl= hostname + "?user=" + userName + "&password=" + password;  
	      con = DriverManager.getConnection(jdbcUrl);

	      return con;
	    }
	    catch (Exception e) { 
	    		e.printStackTrace();
	    }

	    return con;	
	}
	
	
	public void closeConnection(Connection conn, PreparedStatement preStatement, ResultSet rs) {
		if (rs != null)
			try {
				rs.close();
			} catch (SQLException e) {
				
			}
		if (preStatement != null)
			try {
				preStatement.close();
			} catch (SQLException e) {
				
			}
		if (conn != null)
			try {
				conn.close();
			} catch (SQLException e) {
				
			}
	}

}
