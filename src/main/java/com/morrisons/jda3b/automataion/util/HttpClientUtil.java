package com.morrisons.jda3b.automataion.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.morrisons.jda3b.automataion.dto.ResponseDTO;

public class HttpClientUtil {
	
	private static final String APPLICATION_JSON= "application/json";
	private static final String AUTHORIZATION = "Authorization";

	
	
	public ResponseDTO composePOSTRequest(String uri,String body,String auth){
		CloseableHttpClient httpClient = HttpClients.createDefault();
		ResponseDTO rangeResponse = new ResponseDTO();
		CloseableHttpResponse response = null;
        try {
			HttpPost request = new HttpPost(uri);
			StringEntity bodyJson = new StringEntity(body);
			request.setEntity(bodyJson);
			request.setHeader(AUTHORIZATION,auth);
			request.setHeader("Accept", APPLICATION_JSON);
			request.setHeader("Content-type", APPLICATION_JSON);
			response = httpClient.execute(request);
			rangeResponse.setResponseCode(response.getStatusLine().getStatusCode());
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				rangeResponse.setResponseBody(EntityUtils.toString(entity));
			}
		    }catch(Exception ex){
				cleanUp(httpClient, response);

			}
        return rangeResponse;
    }
	
	public void cleanUp(CloseableHttpClient httpClient,CloseableHttpResponse response){
		try {
    		if(httpClient!= null)
        		httpClient.close();
        	if(response!= null)
        		response.close();
		} catch (Exception e) {
			
		}
	}
}
