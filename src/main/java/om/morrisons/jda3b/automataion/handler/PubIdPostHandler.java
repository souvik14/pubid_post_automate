package om.morrisons.jda3b.automataion.handler;

import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.morrisons.jda3b.automataion.config.ApplicationConfig;
import com.morrisons.jda3b.automataion.config.ConfigLoader;
import com.morrisons.jda3b.automataion.processor.ApplicationProcessor;
import com.morrisons.jda3b.automataion.util.HttpClientUtil;
import com.morrisons.jda3b.automataion.util.RDSUtil;



public class PubIdPostHandler implements RequestHandler{
	
	private static Logger logger = LoggerFactory.getLogger(PubIdPostHandler.class);
	private static final ApplicationConfig appConfig = new ConfigLoader().getConfig();
	
	
	public static void main(String[] args){
		//local testing
		String a = "adadas";
		System.out.println("app file ::"+ appConfig.getApiUrl());
		String pubId = "3";System.getenv("pubId");
		String StoreNo = System.getenv("StoreNo");
		RDSUtil dbUtil = new RDSUtil();
		HttpClientUtil clientUtil = new HttpClientUtil();
		ApplicationProcessor procesor = new ApplicationProcessor(appConfig,dbUtil,clientUtil,pubId,StoreNo);
        procesor.processData();
				
	}

	public Object handleRequest(Object input, Context context) {
		
		try{
		String pubId = System.getenv("pubId");
		String StoreNo = System.getenv("StoreNo");
		RDSUtil dbUtil = new RDSUtil();
		HttpClientUtil clientUtil = new HttpClientUtil();
		Instant start = Instant.now();
		ApplicationProcessor procesor = new ApplicationProcessor(appConfig,dbUtil,clientUtil,pubId,StoreNo);
        procesor.processData();
    	Instant end = Instant.now();
		Duration totalTime = Duration.between(start, end);
		logger.info("total time taken:::"+ totalTime.getSeconds());
		}catch(Exception ex){
			logger.error("Exception in Lambda::" + ex.getMessage());
		}
		
		return "ok";
	}

}
